MUX2_1_1bit_inst : MUX2_1_1bit PORT MAP (
		data0	 => data0_sig,
		data1	 => data1_sig,
		sel	 => sel_sig,
		result	 => result_sig
	);
