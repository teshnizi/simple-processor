MUX2_1_4bit_inst : MUX2_1_4bit PORT MAP (
		data0x	 => data0x_sig,
		data1x	 => data1x_sig,
		sel	 => sel_sig,
		result	 => result_sig
	);
