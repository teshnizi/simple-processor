**Design and Implementation of a Simple Processor**

In this project I implemented a small CPU which is capabale of executing simple commands. This processor supports addition and subtraction, loading and storing data, and conditional operations.

Here you can see the main components of the processor:

![](Pictures/DataPath.jpg)


More images could be found in the **Pictures** folder.
